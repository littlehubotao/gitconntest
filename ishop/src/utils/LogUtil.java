package utils;

import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;

public class LogUtil {
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());
	
	//流水登记表
	public static void insertLog() throws SQLException, InterruptedException {
		String uuid = SerialNumberUtils.CreateSerial("ishop", 10);
		System.out.println(uuid);
		String sql_log = "INSERT INTO `ishop`.`tb_log` (`uuid`, `username`, `userid`, `method`, `ip`, `url`, `system`, `loglevel`, `reqdate`, `reqtime`, `rspdate`, `rsptime`, `money`, `info`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				/*+ "("
				+ "'"+ uuid+ "'"+ ", "
				+ "'username', "
				+ "'userid', "
				+ "'method', "
				+ "'ip',"
				+ " 'url', "
				+ "'system', "
				+ "'loglevel', "
				+ "'20191113', "
				+ "'140202', "
				+ "'20191113', "
				+ "'140202', "
				+ "'100', "
				+ "'Ceshi');";*/
			//	"INSERT INTO tb_log (sname,sprice,sdesc)VALUES(?,?,?)";
		//将三个?占位符的实际参数,写在数组中
		//Object[] params = {} ;
		Object[] params = {
				uuid,
				"username",
				"userid",
				"method",
				"ip",
				"url",
				"system",
				"loglevel",
				"20191113",
				"140202",
				"20191113",
				"140202",
				"100",
				"Ceshi"
	};
		int row = qr.update(sql_log,params);//params
		if (row!=0) {
			System.out.println("流水登记成功");
		}else {
			System.out.println("流水登记失败");
		}
		
	}
}
