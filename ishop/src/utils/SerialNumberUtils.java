package utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;

public class SerialNumberUtils {
	
	/***
	 * 生成流水号
	 * @param sysFlg
	 * @param randomCount
	 * @return 流水号
	 * @throws InterruptedException
	 */
	public  synchronized static String CreateSerial(String sysFlg,int randomCount) throws InterruptedException {
		safeSleep(1);
		SimpleDateFormat sdft = new SimpleDateFormat("yyyyMMddhhmmss");
		Date nowdate = new Date();
		String str = sdft.format(nowdate);
		return sysFlg+str+RandomStringUtils.randomNumeric(randomCount);
		
	}
	
	
	public  synchronized  String CreateSerial2(String sysFlg,int randomCount) throws InterruptedException {
		SimpleDateFormat sdft = new SimpleDateFormat("yyyyMMddhhmmss");
		Date nowdate = new Date();
		String str = sdft.format(nowdate);
		return sysFlg+str+RandomStringUtils.randomNumeric(randomCount);
		
	}

	private static void safeSleep(int i) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(i);
	}
	public static void main(String[] args) throws InterruptedException {
		System.out.println(CreateSerial("UPPS", 14));
	}
}
