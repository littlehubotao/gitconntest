package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

public class DBCPUtils {
	private static BasicDataSource dbs = new BasicDataSource();
	private static String className="";
	private static String sqlUrl="";
	private static String sqlUser="";
	private static String sqlPassword="";
	
	 static {
		          initConfig();
		          //设置基本信息
		          dbs.setDriverClassName(className);
		          dbs.setUrl(sqlUrl);
		          dbs.setUsername(sqlUser);
		          dbs.setPassword(sqlPassword);
		          //对连接池控制数--可不配
		          dbs.setInitialSize(10); //连接池初始化连接数
		          dbs.setMaxActive(8); //最大链接数量
		          dbs.setMaxIdle(5); //最大空闲数
		          dbs.setMinIdle(5); //最小空闲数
		      }
	 public static BasicDataSource getDataSource() {
		          return dbs;
		      }
	 
	 
	 private static void initConfig() {
		          InputStream inf = DBCPUtils.class.getClassLoader().getResourceAsStream("database.propertties");
		          Properties properties = new Properties();
		          try {
		              properties.load(inf);
		              className = properties.getProperty("driverClass");
		              sqlUrl = properties.getProperty("url");
		              sqlUser = properties.getProperty("username");
		              sqlPassword = properties.getProperty("password");
		          } catch (IOException e) {
		              e.printStackTrace();
		          }
	 }
}
