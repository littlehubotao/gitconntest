package entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;

public class Tb_user extends BaseRowModel{

    @ExcelProperty(value="ѧ��",index=0)
    private Integer id;
    
    @ExcelProperty(value= {"username"},index=1)
    private String username;
    
    @ExcelProperty(value="password",index=2)
    private String password;

    @ExcelProperty(value="phone",index=2)
    private String phone;

    @ExcelProperty(value="email",index=2)
    private String email;
    
    @ExcelProperty(value="created",index=2)
    private String created;
    
    @ExcelProperty(value="updated",index=2)
    private String updated;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

    

}
