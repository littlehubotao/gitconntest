package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;

import utils.DBCPUtils;

/**
 * Servlet implementation class UserInfoDeleteServlet
 */
//@WebServlet("/UserInfoDeleteServlet")
public class UserInfoDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		 String id = request.getParameter("id");
		 String sql = "delete from tb_user  where id=?";
		 System.out.println(sql);
		 int row;
		 response.setContentType("application/json;charset=utf-8");//response.setContentType("application/json");
		 PrintWriter out = response.getWriter();
		 
		 StringBuilder msg = new StringBuilder();
		try {
			row = qr.update(sql, id);
			System.out.println(row);
			
			 if (row>0) {
		            System.out.println("成功删除用户数据");
		            msg.append("{\"code\":\"0000\","
		        	 		+ "\"msg\":\"成功删除用户数据\"}");
		        }else {
		            System.out.println("删除用户数据失败");
		            msg.append("{\"code\":\"1111\","
		        	 		+ "\"msg\":\"删除用户数据失败\"}");
		        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		 out.append(msg.toString());  
		
		
	}

}
