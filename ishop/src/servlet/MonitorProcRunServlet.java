package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;

import utils.DBCPUtils;
import utils.LogUtil;

/**
 * Servlet implementation class MonitorProcServlet
 */
//@WebServlet("/MonitorProcServlet")
public class MonitorProcRunServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());  
	//日志
	Logger log = Logger.getLogger(MonitorProcRunServlet.class);
	
	public long sum = 0;
	//统计接口调用次数
	public static int num=0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MonitorProcRunServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		//
		 StringBuilder jsonStr = new StringBuilder("");
		//流水入库
		try {
			LogUtil.insertLog();
		       String sql = "call create_user('20100101',1000)";
		       qr.update(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			jsonStr.append("{\"code\":\"0000\","
			 		+ "\"msg\":\"执行存储过程成功\""
			 		+"}");
			 response.setContentType("text/html;charset=utf-8");
			 PrintWriter out = response.getWriter();
			++num;
			log.info("用户查询接口："+"当前接口被调用次数为："+num);
		 System.out.println("rsp:"+jsonStr.toString());
		 out.append(jsonStr.toString());
		
	}
	
	

}
