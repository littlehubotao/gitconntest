package servlet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import utils.DBCPUtils;

public class UserInfoQueryExportServlet_Test {
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());
	//日志
		static Logger log = Logger.getLogger(UserInfoQueryExportServlet_Test.class);
	public static void main(String[] args) throws SQLException, FileNotFoundException, IOException {
		log.info("starttime:"+new Date());
		
		
		StringBuilder sql = new StringBuilder("select * from tb_user ");//
		List<Map<String,Object>> list = qr.query(sql.toString(), new MapListHandler());
		//[{id=28, category_id=89, title=标题1, sub_title=标题1, title_desc=标题1, url=http://www.jd.com, pic=http://localhost:9000/images/2015/07/27/1437979301511057.jpg, pic2=null, content=标题1, created=2015-07-27 14:41:57.0, updated=2015-07-27 14:41:57.0}, {id=29, category_id=89, title=ad2, sub_title=ad2, title_desc=ad2, url=http://www.baidu.com, pic=http://localhost:9000/images/2015/07/27/1437979349040954.jpg, pic2=null, content=ad2, created=2015-07-27 14:42:36.0, updated=2015-07-27 14:42:36.0}, {id=30, category_id=89, title=ad3, sub_title=ad3, title_desc=ad3, url=http://www.sina.com.cn, pic=http://localhost:9000/images/2015/07/27/1437979377450366.jpg, pic2=null, content=, created=2015-07-27 14:42:58.0, updated=2015-07-27 14:42:58.0}, {id=31, category_id=89, title=ad4, sub_title=ad4, title_desc=ad4, url=ad4, pic=http://localhost:9000/images/2015/07/27/1437979392186756.jpg, pic2=null, content=ad4, created=2015-07-27 14:43:15.0, updated=2015-07-27 14:43:15.0}]
		int count = 0;
		//String str = JSON.toJSONString(list); //此行转换
		File file = new File("C:\\Users\\11792\\Desktop\\新建文件夹 (2)\\报表"+"_"+"20191112.xlsx");
		log.info(list.size());
		//log.info(list);
		//创建工作簿
		XSSFWorkbook workbook = new XSSFWorkbook();
		/*String[] data = {"73982", "1", "NH", "1", "2018122510", "2", "0", "0", "0", "12233", "0", "楼层组件优化", "0"};*/
		//创建工作表
		XSSFSheet sheet = workbook.createSheet("用户信息表");
		//tb_content
		//String columnarr[] = {"id","category_id","title","sub_title","title_desc","url","pic2","content","created","updated"};
		//tb_user
		String columnarr[] = {"id","username","password","phone","email","created","updated"};
		//设置数据
		for (int row = 0; row < list.size(); row++) {
			//创建行
			XSSFRow sheetRow = sheet.createRow(row);
			//System.out.println("共有："+columnarr.length+"列");
				//设置列
			for (int column = 0; column < columnarr.length; column++) {
				//System.out.println("row:"+row+"    column:"+column);
				//System.out.println("columnarr[column]:"+columnarr[column]);
				//System.out.println("list.get("+row+").get("+columnarr[column]+").toString()");
				if (list.get(row).get(columnarr[column])!= null) {
					//System.out.println(list.get(row).get(columnarr[column]).toString());
					sheetRow.createCell(column).setCellValue(list.get(row).get(columnarr[column]).toString());
					
				}else {
					//System.out.println("list.get("+row+").get("+columnarr[column]+").toString() 的值是null");
				}
				//写入行数：
				++count;
				System.out.println("当前写入："+count+"行");
				//写入文件 在文件尾部写入数据
				workbook.write(new FileOutputStream(file,true));
			
				//System.out.println("----------------------------------------------");
				/*if (list.get(column).get(columnarr[column]).toString() != null) {
					sheetRow.createCell(row).setCellValue(list.get(column).get(columnarr[column]).toString());
				}*/
			}
		}
		//写入文件
		//workbook.write(new FileOutputStream(new File("C:\\Users\\11792\\Desktop\\新建文件夹 (2)\\test.xlsx")));
		//Exception in thread "main" java.lang.OutOfMemoryError: GC overhead limit exceeded
			//workbook.cloneSheet(0);
		log.info("endtime:"+new Date());
		/*
		 * HSSFWorkbook生成的是xls文件，XSSFWorkbook生成的是xlsx文件（Excel2007版本）
		 * 　HSSFWorkbook:是操作Excel2003以前（包括2003）的版本，扩展名是.xls
　　		  XSSFWorkbook:是操作Excel2007的版本，扩展名是.xlsx
		 * */
		
	}
}
