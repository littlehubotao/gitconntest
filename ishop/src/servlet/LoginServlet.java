package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;

import utils.DBCPUtils;
import utils.SerialNumberUtils;

/**
 * Servlet implementation class LoginServlet
 */
/*@WebServlet("/LoginServlet")*/
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//日志
	Logger log = Logger.getLogger(LoginServlet.class);
	//统计接口调用次数
	public static int num=0;
	
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id =  request.getParameter("id");
		String password =  request.getParameter("password");
		++num;
		try {
			log.info("当前流水号为:"+SerialNumberUtils.CreateSerial("UPPS", 10));
			//接口调用次数
			log.info("请求时间："+new Date()+"当前接口被调用次数为："+num);
		     // StringBuilder sb = new StringBuilder();
		      String sql2 = "select count(*) from tb_user where id  = '"+id+"' and password='"+password+"'";
		      String sql = "select * from tb_user where id  = '"+id+"' and password='"+password+"'";
		      Long  sum  = qr.query( sql2.toString(), new ScalarHandler<Long>());
		      StringBuilder msg = new StringBuilder();
		      List<Map<String,Object>> list = null;
		      if (sum > 0) {
		    	   list = qr.query( sql, new MapListHandler());  
		    	   String username = "";
					for (Map<String, Object> map : list) {
						//System.out.println(map.get("username"));
						username = map.get("username").toString();
						HttpSession session = request.getSession();
						session.setAttribute("username", username);
						//sb.append(map.get("username"));
					}
					System.out.println("查询数据成功");
		            msg.append("{\"code\":\"0000\","
		            		+"\"username\":"+"\""+username+"\","
		        	 		+ "\"msg\":\"查询数据成功\"}");
					
		      }else {
		    	  System.out.println("查询数据失败");
		            msg.append("{\"code\":\"1111\","
		        	 		+ "\"msg\":\"查询数据失败\"}");
			}
				log.info("响应时间："+new Date()+"当前接口被调用次数为："+num);
				 response.setContentType("application/json;charset=utf-8");
				 PrintWriter out = response.getWriter();
				 out.append(msg.toString());   
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//doGet(request, response);
	}

}
