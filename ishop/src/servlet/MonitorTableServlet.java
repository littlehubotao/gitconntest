package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;

import utils.DBCPUtils;
import utils.LogUtil;

/**
 * Servlet implementation class MonitorProcServlet
 */
//@WebServlet("/MonitorProcServlet")
public class MonitorTableServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());  
	//日志
	Logger log = Logger.getLogger(MonitorTableServlet.class);
	public String str = "";
	
	public long sum = 0;
	//统计接口调用次数
	public static int num=0;
	//查询ishop数据库所有的存储过程
	 String sql2 = "SELECT table_name,TABLE_ROWS,TABLE_COMMENT from information_schema.`TABLES` where TABLE_SCHEMA='ishop' ;";
	// String sql2 = "select  * from information_schema.ROUTINES where ROUTINE_SCHEMA='ishop'";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MonitorTableServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		System.out.println("hello---------------");
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		//
		System.out.println("hello----post-----------");
		 StringBuilder jsonStr = new StringBuilder();
		//流水入库
		try {
			LogUtil.insertLog();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		Map<String, Object> map2 = new HashMap<String, Object>();*/
		try {
			List<Map<String,Object>> list = qr.query(sql2, new MapListHandler());
			//sum  = qr.query(sql1, new ScalarHandler<Long>());
			//List<Map<String,Object>> list2 = new ArrayList<Map<String,Object>>;
			/*for (Map<String, Object> map : list) {
				Object SPECIFIC_NAME = map.get("SPECIFIC_NAME");
				Object ROUTINE_SCHEMA = map.get("ROUTINE_SCHEMA");
				Object CREATED = map.get("CREATED");
				Object LAST_ALTERED = map.get("LAST_ALTERED");
				Object DEFINER = map.get("DEFINER");
				System.out.println(map.get("SPECIFIC_NAME"));
				map2.put("SPECIFIC_NAME", SPECIFIC_NAME);
				map2.put("ROUTINE_SCHEMA", ROUTINE_SCHEMA);
				map2.put("CREATED", CREATED);
				map2.put("LAST_ALTERED", LAST_ALTERED);
				map2.put("DEFINER", DEFINER);
				list2.add(map2);
			}*/
			str = JSON.toJSONString(list); //此行转换
			list.clear();
			jsonStr.append("{\"code\":0,"
			 		+ "\"msg\":\"查询成功\","
			 		+"\"count\":"+sum+","//1000,"
			 		+ "\"data\":"
			 		+str+"}");
			 response.setContentType("text/html;charset=utf-8");
			 PrintWriter out = response.getWriter();
			++num;
			log.info("用户查询接口："+"当前接口被调用次数为："+num);
		 System.out.println("rsp:"+jsonStr.toString());
		 out.append(jsonStr.toString());
		 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	
	

}
