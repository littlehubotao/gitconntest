package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;

import utils.DBCPUtils;


/**
 * Servlet implementation class UserInfoUpdateServlet
 */
//@WebServlet("/UserInfoUpdateServlet")
public class UserInfoUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 String id = request.getParameter("id");
		 String username = request.getParameter("username");
		 String password = request.getParameter("password");
		 String phone = request.getParameter("phone");
		 String email = request.getParameter("email");
		 String created = request.getParameter("created");
		 String updated = request.getParameter("updated");
		 System.out.println(id);
		 String sql = "update tb_user set "
		 		+ "username='"+username+"' , "
		 		+ "password='"+password+"' , "
		 		+ "phone='"+phone+"' , "
		 		+ "email='"+email+"' , "
		 		+ "created='"+created+"' , "
		 		+ "updated='"+updated+"'  "
		 		+ " where id="+id;
		 System.out.println(sql);
		 response.setContentType("application/json;charset=utf-8");//response.setContentType("application/json");
		 PrintWriter out = response.getWriter();
	        Object[] obj = {};
	      //  String msg ="";
	        
	   	 StringBuilder msg = new StringBuilder();
	        //"{\"code\":0,"
	 		//+ "\"msg\":\"查询成功\","
	        int res;
			try {
				res = qr.update(sql,obj);
				if (res>0) {
		            System.out.println("更新数据成功");
		            msg.append("{\"code\":\"0000\","
		        	 		+ "\"msg\":\"更新数据成功\"}");
		        }else {
		            System.out.println("更新数据失败");
		            msg.append("{\"code\":\"1111\","
		        	 		+ "\"msg\":\"更新数据失败\"}");
		        }
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 out.append(msg.toString());  
			// request.getRequestDispatcher("admin/userInfoUpdate.jsp").forward(request, response);
		//doGet(request, response);
		
		
	}

}
