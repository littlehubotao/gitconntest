package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;

import utils.DBCPUtils;

/**
 * Servlet implementation class UserServlet
 */
//@WebServlet("/UserServlet")
public class UserInfoQueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());
	//日志
		Logger log = Logger.getLogger(UserInfoQueryServlet.class);
	//统计接口调用次数
		public static int num=0;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoQueryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("这是UserInfoQueryServlet的get方法");
		doPost(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String id = request.getParameter("key[id]");
		if (id == null) {
			 id = request.getParameter("id");
		}
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		int count = 0;
		System.out.println("id:"+id);
		System.out.println("page:"+page);
		System.out.println("limit"+limit);
		StringBuilder sql = new StringBuilder("select * from tb_user where 1=1  ");
		StringBuilder sql2 = new StringBuilder("select count(*) from tb_user where 1=1 ");
		if (null != id && id.trim()!="") {
			/*sql.append("and id = '"+id+"' ");
			sql2.append("and id = '"+id+"' ");*/
			
			sql.append("and id="+id);
			sql2.append("and id="+id);
		}
		if (page == null) {
			
		}else if (page.equals("1")) {
			System.out.println(page);
			sql.append(" limit  0,"+(Integer.valueOf(page))*Integer.valueOf(limit)+";");//limit
		}else {
			sql.append(" limit  "+(Integer.valueOf(page)-1)*Integer.valueOf(limit)+","+Integer.valueOf(page)*Integer.valueOf(limit));//limit
		}
		 long sum = 0;
     	 System.out.println("数据库总条数："+sum); 
    	  
		System.out.println(sql);
		System.out.println(sql2);
		 String str ="";
	      try {
	    	  
	    	sum  = qr.query( sql2.toString(), new ScalarHandler<Long>());
	    	System.out.println("sum:"+sum);
			List<Map<String,Object>> list = qr.query(sql.toString(), new MapListHandler());
			 count = list.size();
			System.out.println("count:"+count);
			//System.out.println(list.toString());
			
			str = JSON.toJSONString(list); //此行转换
			System.out.println("str:"+str);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	     // JSONObject result = new JSONObject();
		
		//System.out.println("1111111111111111111111");
		 response.setContentType("text/html;charset=utf-8");
		 PrintWriter out = response.getWriter();
		 StringBuilder jsonStr = new StringBuilder();
		 if (sum == 0) {
			 jsonStr.append("{\"code\":1,"
		 		+ "\"msg\":\"查询条数是0\","
		 		+"\"count\":"+sum+","//1000,"
		 		+ "\"data\":"
		 		//+ "["
		 		+str+"}");
			 
		}else {
				 jsonStr.append("{\"code\":0,"
			 		+ "\"msg\":\"查询成功\","
			 		+"\"count\":"+sum+","//1000,"
			 		+ "\"data\":"
			 		//+ "["
			 		+str+"}");;
		}
		/* String jsonStr = "{\"code\":0,"
		 		+ "\"msg\":\"查询成功\","
		 		+"\"count\":"+sum+","//1000,"
		 		+ "\"data\":"
		 		//+ "["
		 		+str
		 		+"}";*/
		 	 /* + "{\"id\":10000,\"username\":\"user-0\",\"sex\":\"女\",\"city\":\"城市-0\",\"sign\":\"签名-0\",\"experience\":255,\"logins\":24,\"wealth\":82830700,\"classify\":\"作家\",\"score\":57}," + 
		 		"{\"id\":10001,\"username\":\"user-1\",\"sex\":\"男\",\"city\":\"城市-1\",\"sign\":\"签名-1\",\"experience\":884,\"logins\":58,\"wealth\":64928690,\"classify\":\"词人\",\"score\":27}," + 
		 		"{\"id\":10002,\"username\":\"user-2\",\"sex\":\"女\",\"city\":\"城市-2\",\"sign\":\"签名-2\",\"experience\":650,\"logins\":77,\"wealth\":6298078,\"classify\":\"酱油\",\"score\":31}," + 
		 		"{\"id\":10003,\"username\":\"user-3\",\"sex\":\"女\",\"city\":\"城市-3\",\"sign\":\"签名-3\",\"experience\":362,\"logins\":157,\"wealth\":37117017,\"classify\":\"诗人\",\"score\":68}," + 
		 		"{\"id\":10004,\"username\":\"user-4\",\"sex\":\"男\",\"city\":\"城市-4\",\"sign\":\"签名-4\",\"experience\":807,\"logins\":51,\"wealth\":76263262,\"classify\":\"作家\",\"score\":6}," + 
		 		"{\"id\":10005,\"username\":\"user-5\",\"sex\":\"女\",\"city\":\"城市-5\",\"sign\":\"签名-5\",\"experience\":173,\"logins\":68,\"wealth\":60344147,\"classify\":\"作家\",\"score\":87}," + 
		 		"{\"id\":10006,\"username\":\"user-6\",\"sex\":\"女\",\"city\":\"城市-6\",\"sign\":\"签名-6\",\"experience\":982,\"logins\":37,\"wealth\":57768166,\"classify\":\"作家\",\"score\":34}," + 
		 		"{\"id\":10007,\"username\":\"user-7\",\"sex\":\"男\",\"city\":\"城市-7\",\"sign\":\"签名-7\",\"experience\":727,\"logins\":150,\"wealth\":82030578,\"classify\":\"作家\",\"score\":28}," + 
		 		"{\"id\":10008,\"username\":\"user-8\",\"sex\":\"男\",\"city\":\"城市-8\",\"sign\":\"签名-8\",\"experience\":951,\"logins\":133,\"wealth\":16503371,\"classify\":\"词人\",\"score\":14}," + 
		 		"{\"id\":10009,\"username\":\"user-9\",\"sex\":\"女\",\"city\":\"城市-9\",\"sign\":\"签名-9\",\"experience\":484,\"logins\":25,\"wealth\":86801934,\"classify\":\"词人\",\"score\":75}," + 
		 		"{\"id\":10010,\"username\":\"user-10\",\"sex\":\"女\",\"city\":\"城市-10\",\"sign\":\"签名-10\",\"experience\":1016,\"logins\":182,\"wealth\":71294671,\"classify\":\"诗人\",\"score\":34}," + 
		 		"{\"id\":10011,\"username\":\"user-11\",\"sex\":\"女\",\"city\":\"城市-11\",\"sign\":\"签名-11\",\"experience\":492,\"logins\":107,\"wealth\":8062783,\"classify\":\"诗人\",\"score\":6}," + 
		 		"{\"id\":10012,\"username\":\"user-12\",\"sex\":\"女\",\"city\":\"城市-12\",\"sign\":\"签名-12\",\"experience\":106,\"logins\":176,\"wealth\":42622704,\"classify\":\"词人\",\"score\":54}," + 
		 		"{\"id\":10013,\"username\":\"user-13\",\"sex\":\"男\",\"city\":\"城市-13\",\"sign\":\"签名-13\",\"experience\":1047,\"logins\":94,\"wealth\":59508583,\"classify\":\"诗人\",\"score\":63}," + 
		 		"{\"id\":10014,\"username\":\"user-14\",\"sex\":\"男\",\"city\":\"城市-14\",\"sign\":\"签名-14\",\"experience\":873,\"logins\":116,\"wealth\":72549912,\"classify\":\"词人\",\"score\":8}," + 
		 		"{\"id\":10015,\"username\":\"user-15\",\"sex\":\"女\",\"city\":\"城市-15\",\"sign\":\"签名-15\",\"experience\":1068,\"logins\":27,\"wealth\":52737025,\"classify\":\"作家\",\"score\":28}," + 
		 		"{\"id\":10016,\"username\":\"user-16\",\"sex\":\"女\",\"city\":\"城市-16\",\"sign\":\"签名-16\",\"experience\":862,\"logins\":168,\"wealth\":37069775,\"classify\":\"酱油\",\"score\":86}," + 
		 		"{\"id\":10017,\"username\":\"user-17\",\"sex\":\"女\",\"city\":\"城市-17\",\"sign\":\"签名-17\",\"experience\":1060,\"logins\":187,\"wealth\":66099525,\"classify\":\"作家\",\"score\":69}," + 
		 		"{\"id\":10018,\"username\":\"user-18\",\"sex\":\"女\",\"city\":\"城市-18\",\"sign\":\"签名-18\",\"experience\":866,\"logins\":88,\"wealth\":81722326,\"classify\":\"词人\",\"score\":74}," + 
		 		"{\"id\":10019,\"username\":\"user-19\",\"sex\":\"女\",\"city\":\"城市-19\",\"sign\":\"签名-19\",\"experience\":682,\"logins\":106,\"wealth\":68647362,\"classify\":\"词人\",\"score\":51}," + 
		 		"{\"id\":10020,\"username\":\"user-20\",\"sex\":\"男\",\"city\":\"城市-20\",\"sign\":\"签名-20\",\"experience\":770,\"logins\":24,\"wealth\":92420248,\"classify\":\"诗人\",\"score\":87}," + 
		 		"{\"id\":10021,\"username\":\"user-21\",\"sex\":\"男\",\"city\":\"城市-21\",\"sign\":\"签名-21\",\"experience\":184,\"logins\":131,\"wealth\":71566045,\"classify\":\"词人\",\"score\":99}," + 
		 		"{\"id\":10022,\"username\":\"user-22\",\"sex\":\"男\",\"city\":\"城市-22\",\"sign\":\"签名-22\",\"experience\":739,\"logins\":152,\"wealth\":60907929,\"classify\":\"作家\",\"score\":18}," + 
		 		"{\"id\":10023,\"username\":\"user-23\",\"sex\":\"女\",\"city\":\"城市-23\",\"sign\":\"签名-23\",\"experience\":127,\"logins\":82,\"wealth\":14765943,\"classify\":\"作家\",\"score\":30}," + 
		 		"{\"id\":10024,\"username\":\"user-24\",\"sex\":\"女\",\"city\":\"城市-24\",\"sign\":\"签名-24\",\"experience\":212,\"logins\":133,\"wealth\":59011052,\"classify\":\"词人\",\"score\":76}," + 
		 		"{\"id\":10025,\"username\":\"user-25\",\"sex\":\"女\",\"city\":\"城市-25\",\"sign\":\"签名-25\",\"experience\":938,\"logins\":182,\"wealth\":91183097,\"classify\":\"作家\",\"score\":69}," + 
		 		"{\"id\":10026,\"username\":\"user-26\",\"sex\":\"男\",\"city\":\"城市-26\",\"sign\":\"签名-26\",\"experience\":978,\"logins\":7,\"wealth\":48008413,\"classify\":\"作家\",\"score\":65}," + 
		 		"{\"id\":10027,\"username\":\"user-27\",\"sex\":\"女\",\"city\":\"城市-27\",\"sign\":\"签名-27\",\"experience\":371,\"logins\":44,\"wealth\":64419691,\"classify\":\"诗人\",\"score\":60}," + 
		 		"{\"id\":10028,\"username\":\"user-28\",\"sex\":\"女\",\"city\":\"城市-28\",\"sign\":\"签名-28\",\"experience\":977,\"logins\":21,\"wealth\":75935022,\"classify\":\"作家\",\"score\":37}," + 
		 		"{\"id\":10029,\"username\":\"user-29\",\"sex\":\"男\",\"city\":\"城市-29\",\"sign\":\"签名-29\",\"experience\":647,\"logins\":107,\"wealth\":97450636,\"classify\":\"酱油\",\"score\":27}" 
		 		*/
		 		//+"}";
		 	++num;
			log.info("用户查询接口："+"当前接口被调用次数为："+num);
		 System.out.println("rsp:"+jsonStr.toString());
		 out.append(jsonStr.toString());
		//doGet(request, response);
	}

}
