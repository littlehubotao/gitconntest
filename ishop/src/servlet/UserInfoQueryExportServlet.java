package servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.support.ExcelTypeEnum;

import entity.Tb_user;
import utils.DBCPUtils;
import utils.LogUtil;

/**
 * Servlet implementation class UserInfoQueryExportServlet
 */
//@WebServlet("/UserInfoQueryExportServlet")
public class UserInfoQueryExportServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoQueryExportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		LogUtil log =new LogUtil();
		try {
			//登记流水
			log.insertLog();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			File fileName = new File("hhhhh.xlsx");
		   OutputStream os = new FileOutputStream(fileName);
	       ExcelWriter writer = new ExcelWriter(os, ExcelTypeEnum.XLSX, true);
	       Date date1 = new Date();
	       System.out.println("开始-----"+date1);
	       // 2代表sheetNo,不可以重复,如果两个sheet的sheetNo相同则输出时只会有一个sheet
	       Sheet sheet1 = new Sheet(1, 5, Tb_user.class);
	       sheet1.setSheetName("第一个sheet");

	       Sheet sheet2 = new Sheet(2, 1, Tb_user.class);
	       sheet2.setSheetName("第二个sheet");
	       QueryRunner runner = new QueryRunner(DBCPUtils.getDataSource());
	       //QueryRunner runner = new QueryRunner(JdbcUtils.getDS());
	       String sql = "SELECT * from tb_user";
	       
	       List<Tb_user> result = null;
		try {
			result = runner.query(sql, new BeanListHandler<Tb_user>(Tb_user.class));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       //System.out.println(result.toString());
	      
	       writer.write(result, sheet1);
	       // writer.write(result,sheet2);

	       writer.finish();
	       System.out.println("数据已写出");
	       Date date2 = new Date();
	       System.out.println("开始-----"+date1);
	       System.out.println("jieshu:-----"+date2);
	       
	       
	
	       
		 StringBuilder jsonStr = new StringBuilder();
		 jsonStr.append("{\"code\":0,"
			 		+ "\"msg\":\"查询成功\","
				    + "\"filename\":\""+fileName+"\""
			 		+"}");
			 		//+"}");;
		 System.out.println("-----------------------导出excel-----------------------");
		// System.out.println(jsonStr);
		 System.out.println("-----------------------导出excel-----------------------");
		 System.out.println("endtime:"+new Date());
		 /*response.setContentType("text/html;charset=utf-8");
		 PrintWriter out = response.getWriter();
		 out.append(jsonStr.toString());*/
		 
		 PrintWriter out = response.getWriter();
		 out.append(jsonStr.toString());
		 /*response.setContentType("application/octet-stream;charset=utf-8");
         response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
         response.addHeader("Pargam", "no-cache");
         response.addHeader("Cache-Control", "no-cache");*/
		 
		 //清理buffer缓存
		 response.reset();
		 //发送数据属于excel文件类型
		 response.setContentType("application/vnd.ms-excel;charset=utf-8"); 
		 response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		 response.setHeader("Pargam", "no-cache");
		 response.setHeader("Cache-Control", "no-cache");
		 //编码格式为utf-
		 response.setCharacterEncoding("UTF-8");  
		  
		// response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8");
		 //response.setContentType("multipart/form-data");
		 //response.setContentType("application/msexcel");
		 //attachment作为附件打开 inline直接打开
	     
	     out.close();
	}

}
