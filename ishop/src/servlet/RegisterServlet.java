package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;

import utils.DBCPUtils;

/**
 * Servlet implementation class RegisterServlet
 */
//@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static QueryRunner qr = new QueryRunner(DBCPUtils.getDataSource());   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		 String id = request.getParameter("id");
		 String username = request.getParameter("username");
		 String password = request.getParameter("password");
		 String phone = request.getParameter("phone");
		 String email = request.getParameter("email");
		
		 int res;
		 StringBuilder msg = new StringBuilder();
		 
		 String sql = "insert into tb_user values("
				    +id+" , "
			 		+"'"+username+"' , "
			 		+"'"+password+"' , "
			 		+"'"+phone+"' , "
			 		+"'"+email+"' , "
			 		+"'2015-06-19 10:24:27'"+", "
			 		+"'2015-06-19 10:24:27'"
			 		+");";
		 System.out.println(sql);
		 Object[] obj = {};
		 try {
			res = qr.update(sql,obj);
			if (res>0) {
	            System.out.println("插入数据成功");
	            msg.append("{\"code\":\"0000\","
	        	 		+ "\"msg\":\"插入数据成功\"}");
	        }else {
	            System.out.println("插入数据失败");
	            msg.append("{\"code\":\"1111\","
	        	 		+ "\"msg\":\"插入数据失败\"}");
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 response.setContentType("application/json;charset=utf-8");//response.setContentType("application/json");
		 PrintWriter out = response.getWriter();
		 out.append(msg.toString());   
		 /*response.setContentType("text/html;charset=utf-8");
		 response.sendRedirect("login.jsp");*/
	}

}
