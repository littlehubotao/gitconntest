<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="../layui/css/layui.css"  media="all">
  <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body> 

<div class="demoTable">
  搜索ID：
  <div class="layui-inline">
    <input class="layui-input" name="id" id="dataReload" autocomplete="off">
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
</div>
 
<table class="layui-hide" id="LAY_table_user" lay-filter="user"></table> 
               
          
<script src="../layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
layui.use('table', function(){
  var table = layui.table;
  
  //方法级渲染
  table.render({
    elem: '#LAY_table_user'  //对应table id
    ,url: '${pageContext.request.contextPath}/UserInfoQuery',
    method:"post"
    ,cols: [[
      {checkbox: true, fixed: true}
      ,{field:'id', title: 'ID', width:150, sort: true, fixed: true}
      ,{field:'username', title: '用户名', width:150}
      ,{field:'password', title: '密码', width:300, sort: true}
      ,{field:'phone', title: '手机号码', width:150}
      ,{field:'email', title: '邮箱', width:150}
      ,{field:'created', title: 'created', sort: true, width:150}
      ,{field:'updated', title: 'updated', sort: true, width:150}
    ]]
    ,id: 'testReload' //
    ,page: true
    //,limit:10
    ,height: 310
    
    
  });
  
  
  <script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
  
  var $ = layui.$, active = {
    reload: function(){
      var dataReload = $('#dataReload');//
      //alert( dataReload.val());
      var index = layer.msg("查询中...",{icon:16,time:false,shade:0});
      //执行重载
      table.reload('testReload', {
        page: {
          curr:1 //重新从第 1 页开始
        }
        ,where: {
          key: {
            id: dataReload.val()
          }
        }
      }, 'data');
      layer.close(index);
    }
  };
 
  
  $('.demoTable .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });''
  
  
 /*  
  laypage.render({
	  elem: 'LAY_table_user'
	  ,count: 70 //数据总数，从服务端得到
	  ,jump: function(obj, first){
	    //obj包含了当前分页的所有参数，比如：
	    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
	    console.log(obj.limit); //得到每页显示的条数
	    
	    //首次不执行
	    if(!first){
	      //do something
	    }
	  }
	}); */
  
  
});
</script>

</body>
</html>