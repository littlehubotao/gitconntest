<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>注册页</title>
    <link rel="stylesheet" href="./layui/css/layui.css">
    <link rel="stylesheet" href="./layui/css/style.css">
   <!--  <link rel="icon" href="../frame/static/image/code.png"> -->
</head>
<body>
 
<div class="login-main">
    <header class="layui-elip" style="width: 82%">注册页</header>
 
    <!-- 表单选项 -->
    <form class="layui-form" >
        <div class="layui-input-inline">
            <!-- 用户名 -->
            <div class="layui-inline" style="width: 85%">
                <input type="text" id="id" name="id" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            </div>
        </div>
            <!-- 密码 -->
        <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
                <input type="password" id="password" name="password" required  lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
            </div>
           <!--  
            <div class="layui-inline">
                <i class="layui-icon" id="pri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            错号
            <div class="layui-inline">
                <i class="layui-icon" id="pwr" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div> -->
        </div>
            <!-- 确认密码 -->
        <!-- <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
                <input type="password" id="rpwd" name="repassword" required  lay-verify="required" placeholder="请确认密码" autocomplete="off" class="layui-input">
            </div>
            对号
            <div class="layui-inline">
                <i class="layui-icon" id="rpri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            错号
            <div class="layui-inline">
                <i class="layui-icon" id="rpwr" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div> -->
<!--  <div class="layui-inline" style="width: 85%">
                <input type="text" id="id" name="id" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            </div>
            
            
            <div class="layui-inline" style="width: 85%">
                <input type="text" id="password" name="password" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            </div> -->
 
 		<div class="layui-inline" style="width: 85%">
                <input type="text" id="username" name="username" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            </div>
            
            <div class="layui-inline" style="width: 85%">
                <input type="text" id="phone" name="phone" required  lay-verify="required" placeholder="请输入phone" autocomplete="off" class="layui-input">
            </div>
            
            <div class="layui-inline" style="width: 85%">
                <input type="text" id="email" name="email" required  lay-verify="required" placeholder="请输入email" autocomplete="off" class="layui-input">
            </div>
 
 
        <div class="layui-input-inline login-btn" style="width: 85%">
            <button type="submit" lay-submit lay-filter="sub" class="layui-btn">注册</button>
        </div>
        <hr style="width: 85%" />
        <p style="width: 85%"><a href="login.html" class="fl">已有账号？立即登录</a><a href="javascript:;" class="fr">忘记密码？</a></p>
    </form>
</div>
 
 
<script src="./layui/layui.js"></script>
<!-- <script type="text/javascript">
$(function(){ 
	document.getElementById("id").focus();
});
</script> -->
<script type="text/javascript">
    layui.use(['form','jquery','layer'], function () {
        var form   = layui.form;
       // var  $      = layui.jquery;
        var  $      = layui.$;
        var layer  = layui.layer;
        //添加表单失焦事件
        //验证表单
        $('#user').blur(function() {
            var user = $(this).val();
            //alert(user);
            $.ajax({
                url:'UserInfoQuery',
                type:'post',
                dataType: "json",
                contentType: 'application/x-www-form-urlencoded', 
                data:{
                	id:user
                },
                //验证用户名是否可用
                success:function(data){
                	//alert(data.count);
                    if (data.count == 0) {  //可用
                    	layer.msg('可用');
                    	//document.getElementById("id").focus();
                        /*  $('#ri').removeAttr('hidden');
                        $('#wr').attr('hidden','hidden');  */
                    } else if (data.count >= 1) {
                    	layer.msg('不可用');
                    	//document.getElementById("id").focus();
                    	 /*   $('#wr').removeAttr('hidden');
                        $('#ri').attr('hidden','hidden');  */
					}
                }//success:function
            })//ajax
 	
        });
 
        // you code ...
        // 为密码添加正则验证
       /*  $('#pwd').blur(function() {
                /* var reg = /^[\w]{6,12}$/;
                if(!($('#pwd').val().match(reg))){
                    //layer.msg('请输入合法密码');
                    $('#pwr').removeAttr('hidden');
                    $('#pri').attr('hidden','hidden');
                    layer.msg('请输入合法密码');
                }else {
                    $('#pri').removeAttr('hidden');
                    $('#pwr').attr('hidden','hidden');
                } 
        }); */
 
        //验证两次密码是否一致
       /*  $('#rpwd').blur(function() {
               /*  if($('#pwd').val() != $('#rpwd').val()){
                    $('#rpwr').removeAttr('hidden');
                    $('#rpri').attr('hidden','hidden');
                    layer.msg('两次输入密码不一致!');
                }else {
                    $('#rpri').removeAttr('hidden');
                    $('#rpwr').attr('hidden','hidden');
                };
        }); */
 
        //action="Register" method="post"
        //添加表单监听事件,提交注册信息
        form.on('submit(sub)', function() {
            $.ajax({
                url:'Register',
                type:'post',
                dataType: "json",
                contentType: 'application/x-www-form-urlencoded', 
                data:{
                    "id":$('#id').val(),
                	"username":$('#username').val(),
                	"password":$('#password').val(),
                	"phone":$('#phone').val(),
                	"email":$('#email').val(),
                	"created":$('#created').val(),
                	"updated":$('#updated').val()
                },
                success:function(data){
                    if (data.code == '0000') {
                        layer.msg('注册成功');
                        
                        
                        layer.msg('注册成功，两秒后跳转到登陆界面',{time:2000},function() {
                        	//回调
                        	 location.href = "login.jsp";
                        	})
                        
                        
                       
                    }else {
                        layer.msg('注册失败');
                    }
                }
            })
            //防止页面跳转
            return false;
        });
 
    });
</script>
</body>
</html>