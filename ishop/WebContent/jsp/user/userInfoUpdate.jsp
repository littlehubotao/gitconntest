<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Layui</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  
  
   <!-- jquery放在前面，否则识别不了$ -->
   <script src="${pageContext.request.contextPath}/layui/jquery-2.1.0.js"></script>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">
   <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/modules/layer/default/layer.css"  media="all">
  <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
  
 
</head>


<script type="text/javascript">
$(function () {
   /*  //从父层获取值，json是父层的全局js变量。eval是将该string类型的json串变为标准的json串
    var parent_json = eval('('+parent.json+')');
    console.log(parent_json); 
    //console.log(parent_json.id); 
    //alert(JSON.stringify(parent_json));
    //layer.alert("后台："+parent_json.id);
    var id = parent_json.id;
    var username = parent_json.username;
    var password = parent_json.password;
    var phone = parent_json.phone;
    var email = parent_json.email;
    var created = parent_json.created;
    var updated = parent_json.updated;
    console.log(username);  */
    
  
});
</script>
<body>
              
<!-- <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
  <legend>用户信息维护</legend>
</fieldset> -->
 

<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/UserInfoUpdate"  method="post" lay-filter="formTest">
  <div class="layui-form-item" id="id">
    <label class="layui-form-label">id</label>
    <div class="layui-input-inline">
      <input type="text" name="id" autocomplete="off" class="layui-input id">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">username</label>
    <div class="layui-input-inline">
      <input type="text" name="username" lay-verify="required" placeholder="请输入username" autocomplete="off" class="layui-input username">
    </div>
  </div>
  
    </div>
  <div class="layui-form-item">
    <label class="layui-form-label">password</label>
    <div class="layui-input-inline">
      <input type="text" name="password" lay-verify="required" placeholder="请输入password" autocomplete="off" class="layui-input password">
    </div>
  </div>
  
  
    <div class="layui-form-item">
    <label class="layui-form-label">phone</label>
    <div class="layui-input-inline">
      <input type="text" name="phone"  placeholder="请输入phone" autocomplete="off" class="layui-input  phone">
    </div>
  </div>
  
  
    <div class="layui-form-item">
    <label class="layui-form-label">email</label>
    <div class="layui-input-inline">
      <input type="text" name="email"  placeholder="请输入email" autocomplete="off" class="layui-input email">
    </div>
  </div>
  
   <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">created</label>
      <div class="layui-input-block">
        <input type="text" name="created" id="date" autocomplete="off" class="layui-input created">
      </div>
    </div>
    
  </div>
  
  <div class="layui-form-item">
    <div class="layui-inline">
      <label class="layui-form-label">updated</label>
      <div class="layui-input-block">
        <input type="text" name="updated" id="date1" autocomplete="off" class="layui-input updated">
      </div>
    </div>
  </div>
  
  <div class="layui-form-item">
    <button class="layui-btn" lay-submit="" lay-filter="update">跳转式提交</button>
  </div>
</form>
          
<script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
layui.use(['form', 'layedit', 'laydate','layer'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
  
  //日期
  laydate.render({
    elem: '#date'
    ,type: 'datetime'
  });
  laydate.render({
    elem: '#date1'
    ,type: 'datetime'
  });
  
  //创建一个编辑器
  var editIndex = layedit.build('LAY_demo_editor');
 
  //自定义验证规则
  form.verify({
    title: function(value){
      if(value.length < 5){
        return '标题至少得5个字符啊';
      }
    }
    ,pass: [
      /^[\S]{6,12}$/
      ,'密码必须6到12位，且不能出现空格'
    ]
    ,content: function(value){
      layedit.sync(editIndex);
    }
  });
  
/*   //监听指定开关
  form.on('switch(switchTest)', function(data){
    layer.msg('开关checked：'+ (this.checked ? 'true' : 'false'), {
      offset: '6px'
    });
    layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
  }); */
  
  //监听提交  
   form.on('submit(update)', function(data){
    /*  layer.alert(JSON.stringify(data.field), {
      title: '最终的提交信息'
    })  */
   // alert($('.id').val());
     $.ajax({
                    url:'${pageContext.request.contextPath}/UserInfoUpdate',
                    type:'post',
                    dataType: "json",
                    contentType: 'application/x-www-form-urlencoded', //application/x-www-form-urlencoded application/json
                    data:{
                    	"id":$('.id').val(),
                    	"username":$('.username').val(),
                    	"password":$('.password').val(),
                    	"phone":$('.phone').val(),
                    	"email":$('.email').val(),
                    	"created":$('.created').val(),
                    	"updated":$('.updated').val()
                    },//JSON.stringify(data),
                    timeout:2000,
                   /*  beforeSend:function (xhr) {
                        xhr.setRequestHeader(header,token);
                    }, */
                    success:function(data){
                        //console.log(data);
                        //alert(data);
                    	 if(data.code == '0000'){
                             //layer.msg("success:function-用户信息修改成功");
                             layer.msg('success:function-用户信息修改成功',{time:1000},function() {
                            	//回调
                            	 var index = parent.layer.getFrameIndex(window.name);  
                                 parent.layer.close(index);//关闭当前页
                                 window.parent.location.reload(); 
                            	})
                            
                             //location.href = "/user/loginpage"
                         }else{
                             layer.msg("success:function-用户信息修改失败");
                         } 
                       
                        //关闭当前frame
                        //parent.layer.close(index);
                        //layer.msg("用户信息修改成功");
                        
                    },
                    error:function (data) {
                    	// alert(data);
                        layer.msg("error:function-用户信息修改失败")
                    }
                })
    
    
    
    return false;
  });
 
  //表单赋值
  /* layui.$('#LAY-component-form-setval').on('click', function(){
    form.val('example', {
      "username": "贤心" // "name": "value"
      ,"password": "123456"
      ,"interest": 1
      ,"like[write]": true //复选框选中状态
      ,"close": true //开关状态
      ,"sex": "女"
      ,"desc": "我爱 layui"
    });
  }); */
  
/*   //表单取值
  layui.$('#LAY-component-form-getval').on('click', function(){
    var data = form.val('example');
    alert(JSON.stringify(data));
  }); */
  
  
});
</script>

</body>
</html>