<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Layui</title>
  <!-- <meta name="renderer" content="webkit"> -->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> -->
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> -->
  
  
  
  
   <!-- jquery放在前面，否则识别不了$ -->
   <script src="${pageContext.request.contextPath}/layui/jquery-2.1.0.js"></script>
   <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/modules/layer/default/layer.css"  media="all">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css"  media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
  <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
 <!--  <script src="../layui/jquery-2.1.0.js"></script> -->
 <!--  <script src="../layui/lay/modules/layer.js"></script> -->
  <script src="${pageContext.request.contextPath}/layui/layui.js" charset="utf-8"></script>
  <script src="${pageContext.request.contextPath}/layui_exts/excel.js" charset="utf-8"></script>
  <%-- <script src="${pageContext.request.contextPath}/layui_exts/excel.min.js" charset="utf-8"></script> --%>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
  
  
  
  
  
</head>
<body>  
<!-- <div style="margin-bottom: 5px;">          
 
示例-970
<ins class="adsbygoogle" style="display:inline-block;width:970px;height:90px" data-ad-client="ca-pub-6111334333458862" data-ad-slot="3820120620"></ins>
 
</div> -->
 
<!-- <div class="layui-btn-group demoTable">
  <button class="layui-btn" data-type="getCheckData">获取选中行数据</button>
  <button class="layui-btn" data-type="getCheckLength">获取选中数目</button>
  <button class="layui-btn" data-type="isAll">验证是否全选</button>
</div> -->
 <div class="demoTable">
  搜索ID：
  <div class="layui-inline">
    <input class="layui-input" name="id" id="demoReload" autocomplete="off">
  </div>
  <button class="layui-btn" data-type="reload">搜索</button>
  
  <!-- ajax -->
  <!-- <button type="button" lay-submit="" class="layui-btn layui-btn-warm" lay-filter="uploadImg">
    <i class="layui-icon"></i>导出Excel</button> -->
  
  <!-- 普通 -->  
  <button type="button" lay-submit="" class="layui-btn layui-btn-warm" lay-filter="" id="exportExcel"><!--  onclick="exportExcel() -->
    <i class="layui-icon"></i>导出Excel</button>
    
</div>
<table class="layui-table" lay-data="{width: 1600, height:480, url:'${pageContext.request.contextPath}/UserInfoQuery',page:true, id:'idTest'}" lay-filter="demo">
  <thead>
    <tr>
      <!-- <th lay-data="{type:'checkbox', fixed: 'left'}"></th> -->
      <th lay-data="{field:'id', width:70, sort: true, fixed: true}">ID</th>
      <th lay-data="{field:'username',width:150, sort: true}">用户名</th>
      <th lay-data="{field:'password',width:300, sort: true}">password</th>
      <th lay-data="{field:'phone', width:150, sort: true}">phone</th>
      <th lay-data="{field:'email', width:150}">email</th>
      <th lay-data="{field:'created', width:150}">created</th>
      <th lay-data="{field:'updated', width:150, sort: true}">updated</th>
      
      <!-- <th lay-data="{field:'classify', width:80}">职业</th>
      <th lay-data="{field:'wealth', width:135, sort: true}">财富</th>
      <th lay-data="{field:'score', width:80, sort: true, fixed: 'right'}">评分</th> -->
      <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barDemo'}"></th>
    </tr>
  </thead>
</table>
 
<script type="text/html" id="barDemo">
  <!--<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>-->
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
               
     


<script>
var json;
layui.use(['layer','table','layer','form','jquery'], function(){
  var table = layui.table;
  var layer=layui.layer;
  var form=layui.form;
  
  var $=layui.$;
  
  

  
  //监听表格复选框选择
  table.on('checkbox(demo)', function(obj){
    console.log(obj)
  });
  //监听工具条
  table.on('tool(demo)', function(obj){
    var data = obj.data;
    if(obj.event === 'detail'){
      layer.msg('ID：'+ data.id + ' 的查看操作');
    } else if(obj.event === 'del'){
      layer.confirm('真的残忍删除该用户信息么？', function(index){
        obj.del();
        layer.close(index);
       // /*
        $.ajax({
            url:'${pageContext.request.contextPath}/UserInfoDelete',
            type:'post',
            dataType: "json",
            contentType: 'application/x-www-form-urlencoded', //application/x-www-form-urlencoded application/json
            data:{
            	"id":data.id
            },//JSON.stringify(data),
            timeout:2000,
            success:function(data){
                //console.log(data);
                //alert(data);
            	 if(data.code == '0000'){
                     //layer.msg("success:function-用户信息修改成功");
                     layer.msg('success:function-用户信息删除成功',{time:1000},function() {
                    	//回调
                    	
                    	})
                    
                     //location.href = "/user/loginpage"
                 }else{
                     layer.msg("success:function-用户信息删除失败");
                 } 
               
                //关闭当前frame
                //parent.layer.close(index);
                //layer.msg("用户信息修改成功");
                
            },
            error:function (data) {
            	// alert(data);
                layer.msg("error:function-用户信息删除失败")
            }
        })


//return false;
//});
     //  */ 
        
        
        
        
      });
    } else if(obj.event === 'edit'){
    // layer.alert(data.id);  //data即为选中行的值
    // layer.alert(rowselect);
    json = JSON.stringify(data); //将行数据传过去
        layer.open({
    		type: 2,
    		title: '用户信息维护',
    		shadeClose: false,
    		shade: 0.5,
    		area: ['60%','70%'], // 长 * 宽
    		content: 'userInfoUpdate.jsp',
    		closeBtn:1,  //右上角关闭
    		success: function (layero, index) {    //成功获得加载userInfoUpdate.jsp时，预先加载，将值从父窗口传到 子窗口
            	var body = layer.getChildFrame('body',index);
    			body.find(".id").val(data.id);
    			body.find(".username").val(data.username);
    			body.find(".password").val(data.password);
    			body.find(".phone").val(data.phone);
    			body.find(".email").val(data.email);
    			body.find(".created").val(data.created);
    			body.find(".updated").val(data.updated);
    			
    			form.render;
    			// layui.form.render();
               /*  var div = layero.find('iframe').contents().find('#id'); 
                var iframeWindow = window['layui-layer-iframe'+ index];
                body.find("#id").val("admin");  */
                //alert("yes");
            }, 
    		end: function () {
    		//关闭弹框后事件 
    		}
    		});     
       
      
    }
  });
  
  var $ = layui.$, active = {
    getCheckData: function(){ //获取选中数据
      var checkStatus = table.checkStatus('idTest')
      ,data = checkStatus.data;
      layer.alert(JSON.stringify(data));
    }
    ,getCheckLength: function(){ //获取选中数目
      var checkStatus = table.checkStatus('idTest')
      ,data = checkStatus.data;
      layer.msg('选中了：'+ data.length + ' 个');
    }
    ,isAll: function(){ //验证是否全选
      var checkStatus = table.checkStatus('idTest');
      layer.msg(checkStatus.isAll ? '全选': '未全选')
    }
  };
  
  $('.demoTable .layui-btn').on('click', function(){
    var type = $(this).data('type');
    active[type] ? active[type].call(this) : '';
  });
  
  
var $ = layui.$, active = {
	    reload: function(){
	        var demoReload = $('#demoReload');
	        
	        //执行重载
	        table.reload('idTest', {
	          page: {
	            curr: 1 //重新从第 1 页开始
	          }
	          ,where: {
	            key: {
	              id: demoReload.val()
	            }
	          }
	        }, 'data');
	      }
	    };
	    
	    
    
	    
//导出excel    
form.on('submit(uploadImg)', function(data){
	//alert("111");
    loading = layer.load(1, {shade: [0.3, '#fff']});
    var $ = layui.jquery;
    var excel = layui.excel;
    $.ajax({
        url: '${pageContext.request.contextPath}/UserInfoQueryExport',
       // dataType: 'json',
        type:'get',
        /* data: {
            datas:JSON.stringify(data.field)
        }, */
        success: function(res) {
        	//alert(res.data);
            layer.close(loading);
            window.location.href = res.filename;
            layer.msg("导出完成！");
            // 假如返回的 res.data 是需要导出的列表数据
           // console.log(res.data);//
         /*   
         layui导出excel
            优点：方便，代码量少
         	缺点：页面无响应，前端oom
         
         // 1. 数组头部新增表头
           res.data.unshift({id: 'ID',username: '用户名',password:'password',phone:'phone',email:'email',created:'created',updated:'updated'});
            // 3. 执行导出函数，系统会弹出弹框
            excel.exportExcel({
                sheet1: res.data
            }, '用户信息.xlsx', 'xlsx'); */
        },
        error:function(res){
            layer.close(loading);
            layer.msg("导出失败");
        }
    });
});


  

  
});
</script>
<script type="text/javascript">
$("#exportExcel").click(function(){
	loading = layer.load(1, {shade: [0.3, '#fff']});
	//window.open('${pageContext.request.contextPath}/UserInfoQueryExport');
	window.location.href='${pageContext.request.contextPath}/UserInfoQueryExport';
})
/* function exportExcel(){
	//alert("111");
	//layer.msg('点击了导出报表');
	loading = layer.load(1, {shade: [0.3, '#fff']});
	window.location.href='${pageContext.request.contextPath}/UserInfoQueryExport';
	//layer.close(loading);
	//layer.msg("导出完成！");
	
} ;  	 */

</script>  
</body>
</html>