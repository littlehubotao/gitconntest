<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>ishop后台</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/layui/css/layui.css">
  <script src="${pageContext.request.contextPath}/layui/layui.js"></script>
</head>
<body class="layui-layout-body">

  		<%  
        String username = (String)request.getSession().getAttribute("username");  
        %>  

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>  

<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
    <div class="layui-logo">ishop后台</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
      <li class="layui-nav-item"><a href="">控制台</a></li>
      <li class="layui-nav-item"><a href="">商品管理</a></li>
    <!--   <li class="layui-nav-item"><a href="userInfo2.jsp" target="option">用户</a></li> -->
      <li class="layui-nav-item">
        <a href="javascript:;">其它系统</a>
        <dl class="layui-nav-child">
          <dd><a href="">邮件管理</a></dd>
          <dd><a href="">消息管理</a></dd>
          <dd><a href="">授权管理</a></dd>
        </dl>
      </li>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item">
        <a href="javascript:;">
          <img src="${pageContext.request.contextPath}/layui/images/pic/hubotao.jpg" class="layui-nav-img">
          <%=username%>  
        </a>
        <dl class="layui-nav-child">
          <dd><a href="">基本资料</a></dd>
          <dd><a href="">安全设置</a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="">退了</a></li>
    </ul>
  </div>
  
  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree"  lay-filter="test">
       
       <!-- 超级管理员 -->
        <li class="layui-nav-item  layui-nav-itemed"><!-- layui-nav-itemed 默认打开 -->
          <a href="javascript:;">运维监控</a>
          <dl class="layui-nav-child">
            <dd><a href="${pageContext.request.contextPath}/jsp/monitor/monitor_Sql.jsp" target="option">表监控</a></dd>
            <dd><a href="${pageContext.request.contextPath}/jsp/monitor/xxx.jsp" target="option">SQL监控</a></dd>
            <dd><a href="${pageContext.request.contextPath}/jsp/monitor/monitor_Procedure.jsp" target="option">调用存储过程</a></dd>
            <dd><a href="${pageContext.request.contextPath}/jsp/monitor/xxx.jsp	" target="option">调用触发器</a></dd>
            <dd><a href="javascript:;">定时调度</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a class="" href="javascript:;">角色权限</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">用户组管理</a></dd>
            <dd><a href="${pageContext.request.contextPath}/jsp/user/userInfo2.jsp" target="option">用户管理</a></dd>
             <!--   <li class="layui-nav-item"><a href="userInfo2.jsp" target="option">用户管理</a></li> -->
            <dd><a href="javascript:;">权限管理</a></dd>
            <dd><a href="javascript:;">标签管理</a></dd>
          </dl>
        </li>
        
        
         <li class="layui-nav-item">
          <a href="javascript:;">数据统计</a>
          <dl class="layui-nav-child">
          	<dd><a href="../user/userInfoStatistics.jsp" target="option">用户数据统计</a></dd>
            <dd><a href="javascript:;">交易数据统计</a></dd>
            <dd><a href="javascript:;">商家数据统计</a></dd>
          </dl>
        </li>
        
         <li class="layui-nav-item">
          <a href="javascript:;">参数管理</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">菜单参数管理</a></dd>
            <dd><a href="javascript:;">xx参数管理</a></dd>
            <dd><a href="javascript:;">xx参数管理</a></dd>
          </dl>
        </li>
        
        <!-- 我是商家 -->
        <li class="layui-nav-item">
          <a href="javascript:;">商品信息</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">商品信息发布</a></dd>
            <dd><a href="javascript:;">商品规格</a></dd>
            <dd><a href="javascript:;">商品分类</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">我是商家</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">店铺信息管理</a></dd>
            <dd><a href="javascript:;">商品管理</a></dd>
            <dd><a href="javascript:;">订单管理</a></dd>
          </dl>
          </li>
          
         
        <!-- 我是客户 -->  
        
         <!-- <li class="layui-nav-item">
          <a href="javascript:;">我是客户</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;">店铺信息管理</a></dd>
            <dd><a href="javascript:;">商品管理</a></dd>
            <dd><a href="javascript:;">订单管理</a></dd>
          </dl>
        </li> -->
       <!--   
        <li class="layui-nav-item"><a href="">云市场</a></li>
        <li class="layui-nav-item"><a href="">发布商品</a></li> -->
      </ul>
    </div>
  </div>
  
  <div class="layui-body">
    <!-- 内容主体区域 -->
    <!-- <div style="padding: 15px;">内容主体区域</div> -->
     <iframe id="option" name="option" src="${pageContext.request.contextPath}/jsp/admin/main.jsp" style="overflow: visible;"frameborder="1" width="100%" height="100%"></iframe>
  </div>
  
  <div class="layui-footer">
    <!-- 底部固定区域 -->
    © www.hubotao.top - 底部固定区域
  </div>
</div>

<script>
//JavaScript代码区域
layui.use('element', function(){
  var element = layui.element;
  
});
</script>
</body>
</html>