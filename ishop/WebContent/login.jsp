<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>ishop-登陆页面</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="./layui/css/layui.css"  media="all">
    <link rel="stylesheet" href="./layui/css/style.css">
  <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
          
     
<div class="login-main">
    <header class="layui-elip" style="width: 82%">登陆页</header>
 
    <!-- 表单选项 -->
    <form class="layui-form">
        <div class="layui-input-inline">
 
 		<div class="layui-inline" style="width: 85%">
                <input type="text" id="id" name="id" required  lay-verify="required" placeholder="请输入id" autocomplete="off" class="layui-input">
            </div>
            
            <div class="layui-inline" style="width: 85%">
                <input type="text" id="password"  name="password" required  lay-verify="required" placeholder="请输入password" autocomplete="off" class="layui-input">
            </div>
            
 
        <div class="layui-input-inline login-btn" style="width: 85%">
            <button type="submit" lay-submit lay-filter="sub" class="layui-btn">登陆</button>
        </div>
 <!-- 
        <div class="layui-form-item">
    <button class="layui-btn" lay-submit="" lay-filter="">跳转式提交</button>
  </div> -->
        <hr style="width: 85%" />
         <p style="width: 85%"><a href="register.jsp" class="fl">未有账号？立即注册</a><a href="javascript:;" class="fr">忘记密码？</a></p>    
    
    
    </form>
</div>  
   <!--     
    <div class="layui-row layui-col-space5">
    
    
    
     左 
    <div class="layui-col-md4">
      <div class="grid-demo grid-demo-bg1">1/3</div>
    </div>
    
    
    
     中 
    <div class="layui-col-md4">
      <div class="grid-demo">1/3</div>
     
                 

<form class="layui-form" action="LoginServlet" method="post">
 
  <div class="layui-form-item">
    <label class="layui-form-label">账号</label>
    <div class="layui-input-block">
      <input type="text" name="id" lay-verify="required" lay-reqtext="账号是必填项，岂能为空？" placeholder="请输入" autocomplete="off" class="layui-input">
    </div>
  </div>
  
 <div class="layui-form-item">
    <label class="layui-form-label">密码</label>
    <div class="layui-input-inline">
      <input type="password" name="password" lay-verify="pass" placeholder="请输入密码" autocomplete="off" class="layui-input">
    </div>
    <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
  </div>
  
  <div class="layui-form-item">
    <button class="layui-btn" lay-submit="" lay-filter="">跳转式提交</button>
  </div>
</form>
  <p style="width: 85%"><a href="register.jsp" class="fl">未有账号？立即注册</a><a href="javascript:;" class="fr">忘记密码？</a></p>    
 右    
    </div>
    <div class="layui-col-md4">
      <div class="grid-demo grid-demo-bg1">1/3</div>
    </div>
  </div>    
        -->
  
          
<script src="./layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
 layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate; 
    var  $      = layui.$;
  
  /* //日期
  laydate.render({
    elem: '#date'
  });
  laydate.render({
    elem: '#date1'
  }); */
  
  //创建一个编辑器
 /*  var editIndex = layedit.build('LAY_demo_editor'); */
 
  //自定义验证规则
 /*  form.verify({
    title: function(value){
      if(value.length < 5){
        return '标题至少得5个字符啊';
      }
    }
     ,pass: [
      /^[\S]{6,12}$/
      ,'密码必须6到12位，且不能出现空格'
    ] 
    ,content: function(value){
      layedit.sync(editIndex);
    }
  });
   */
  //监听指定开关
 /*  form.on('switch(switchTest)', function(data){
    layer.msg('开关checked：'+ (this.checked ? 'true' : 'false'), {
      offset: '6px'
    });
    layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
  }); */
  
  //监听提交
  /* form.on('submit(demo1)', function(data){
    layer.alert(JSON.stringify(data.field), {
      title: '最终的提交信息'
    })
    return false;
  });
  */
  //表单赋值
 /*  layui.$('#LAY-component-form-setval').on('click', function(){
    form.val('example', {
      "username": "贤心" // "name": "value"
      ,"password": "123456"
      ,"interest": 1
      ,"like[write]": true //复选框选中状态
      ,"close": true //开关状态
      ,"sex": "女"
      ,"desc": "我爱 layui"
    });
  }); */
  
  //表单取值
/*   layui.$('#LAY-component-form-getval').on('click', function(){
    var data = form.val('example');
    alert(JSON.stringify(data));
  }); */
   form.on('submit(sub)', function() {
	  // alert("111");
       $.ajax({
           url:'LoginServlet',
           type:'post',
           dataType: "json",
           contentType: 'application/x-www-form-urlencoded', 
           data:{
               "id":$('#id').val(),
           	"password":$('#password').val()
           },
           success:function(data){
               if (data.code == '0000') {
                  // layer.msg('注册成功');
                 /*  alert(data.username); */
                   layer.msg('登陆成功，两秒后跳转到后台界面',{time:2000},function() {
                   	//回调
                   	 location.href = "${pageContext.request.contextPath}/jsp/admin/index.jsp";
                   	})
               }else {
            	   layer.msg('登陆失败',{time:2000},function() {
                      	})
               }
           }
       })
       //防止页面跳转
       return false;
   });

});
</script>

</body>
</html>